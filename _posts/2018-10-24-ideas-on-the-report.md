---
layout: post
title: "Ideas on the Report"
categories: report
---

24 October 2018
By: Diderik van Wingerden

This post contains a 'brain-dump' of thoughts on the Re:INVENT Report.

## Why document and publish about Open Design initiatives?

A many times heard issue with commons-based initiatives is that it is so difficult to know and keep track of all that is going on. This is because of the very nature of these bottom-up, local, grass-roots, distributive, non-centralized initiatives. 

If we believe that it is valuable to know about initiatives and the people behind them, for example to to be inspired by, learn from or collaborate with, then an effort has to be made to keep connecting and getting to know each other and each others' work. 

One approach of doing that is to collect, document, organize and publish knowledge about these initiatives. This approach is amongst others being taken by the Creative Commons and Open Data movements, with their respective reports: [State of the Commons](https://stateof.creativecommons.org/) and the [Global Open Data Index](https://index.okfn.org/) (GODI).

We need to recognize that creating an updated overview, database, catalog, website or report of Open Design initiatives has been tried many times before. All these creations had their value at the time, but the fact is that they have become outdated and are not actively maintained (anymore). There is currently no 'go-to-place' to find out about current and active Open Design initiatives relating to creating a Safe and Just Space for Humanity.

## Why a Report and for whom?

Creating an overview of contemporary Open Design initiatives in the form of a (yearly recurring) report not just follows the good practice of Creative Commons and Open Knowledge Foundation, but has the following advantages:
- Keeping an overview or catalog continuously up-to-date has proven to be problematic without a continuously dedicated (and funded) organization or team behind it. Creating a report is a 1-time project-based thing that is easier to fund and manage as it is clear in scope and time.
- A report allows for more in-depth information and analysis than a mere list of initiatives. This makes insights and learnings more readily available. Creating multiple reports over time also allows for historic trend analysis.
- Creating a visually attractive and insightful report not only is valuable for the 'insiders', the people already familiar with Open Design and related concepts, but offers the opportunity to advocate the value of creating solutions based on Open Design principles to a wider audience to the benefit of the world. Such audiences could amongst others be: users, the people who can directly benefit from using the Open Design solutions, workers in Humanitarian Aid and Humanitarian Development, (social) entrepreneurs and impact investors.

Along with an (online and offline) report an online searchable database can be created holding all information about the Open Design initiatives as included in the report. At first this database would be updated yearly, coinciding with creating the yearly report. But there is also an opportunity that with sufficient visibility and usefulness the online database can benefit from a bandwagon effect and the community starts to update and maintain the information organically.

## How to create the Report?

A 'ballpark estimate' has been on how much time and money is needed to create such a report. Based on interviews with people from Creative Commons, from former Open Knowledge Foundation and some educated guesses, this ballpark estimate for the required budget is: between EUR 30,000 and EUR 50,000. This includes the time for gathering quantitative and qualitative data, copywriting, communication and coordination and report design, based on an hourly tariff of EUR 75. See [this PDF](/media/osworld-2018-10-22.pdf) for the full analysis, also available in [editable source](https://board.net/p/osworld).

The traditional way to create a report like this is that a small team of people get together, sit down, close the door, muscle through for several months and then come out with a new and shiny report. If we are to advocate openness in result as well as process, we believe that creating an Open Design Report should in itself also be an open process. As far as we understood the creation of the GODI report is at least partly open, as community members from various countries work together to gather input in a structured way and the draft report is sent out for review. However, as far as we understood much of the 'inner workings' of the team is still behind closed doors and not documented, transparent and repeatable by others. 

The approach for the Open Design Report should be to strive to complete openness, transparency and deliver step-by-step process documentation so it can be repeated by others without inside knowledge and permission. Taking this approach will be a learning experience in itself and can be a valuable contribution to the commons: creating a community-based report in an open, distributed, transparent, documented and repeatable way. Coming to a complete open process will probably take several 'rounds' of the report, so while striving for complete openness it is best to take a pragmatic approach and each time do what can reasonably be done with the available time and budget.

Some tasks in the report creation process that are candidates to be reinvented using an open approach:
- Establishing the SPQ ("Situation, Problem, Question"), main message, goal and audience(s) of the report (why, what, for whom, ...)
- Establishing the (various) form the report will take: online and/or offline
- Designing criteria of which kind of Open Design initiatives are included
- Designing a step-by-step process of how candidate initiatives are gathered
- Establishing how candidate initiatives are validated against the inclusion criteria and how this is documented
- Designing which attributes of Open Design initiatives are interesting to measure (quantitatively and qualitatively)
- Designing a step-by-step process of how information on initiatives is gathered and validated
- Researching, experimenting with, possibly adapting/building and deciding on tools to use for communication, documentation, data gathering, data storage, analysis, etc.
- Designing and creating a 'datastore' for all gathered data, including how to input, edit, remove data
- Designing interesting analyses that can be done on the data (qualitative and quantitative)
- Performing the data analyses, writing and creating the results, copywriting
- Copywriting of things not directly related to the data, like an introduction, author profiles, explanation of common concepts
- Designing the report style in each form
- Creating the actual report in each form, merging the created copy and design
- Gathering feedback on the draft of the report
- Creating a 'marketing and communication' plan for publishing the report
- Publishing the report in each form
- Executing on the marketing and communication plan
- Keeping the community updated on progress, current challenges, asking for help

When creating the report and working on the above tasks an iterative approach should be taken. This means that the process should not be executed ['waterfall style'](https://en.wikipedia.org/wiki/Waterfall_model) as this will prevent any possibility of learning along the way. Instead, we should understand that much is to be learned and it has to be possible to revisit previous (design) decisions and do rework to come to an optimal result.

## Standing on the shoulders of giants

To benefit from the work and learnings of others and to further augment the open approach of creating the report, it is valuable to consider reusing existing open documents, designs, definitions and tools. These are for example: the work of the Open Source Hardware Association (OSHWA), including the Open Source Hardware Definition and Certification Directory and the work of the [OPEN! Research Project](https://opensourcedesign.cc/), including the Open-O-Meter and the Observatory of Open Source Hardware. In that way we can 'stand on the shoulders of giants' and by open licensing any modifications and additional work pay it forward.

## Things to be discussed

Besides that the above ideas are naturally open for discussion, a list of questions with arbitrary answers and therefore open for discussion:

**Can an Open Design initiative only be included if it is "Open Source" according to the generally accepted OSHWA definition?**
Practice shows that many initiatives calling themselves "Open Source" are not actually Open Source according to the generally accepted definition. For example due to lacking proper open licensing, deliberately choosing a licenses incompatible with Open Source (such as a Non-Commercial license), having the designs and documentation 'behind closed doors' only available to insiders or vetted community members or not having designs and documentation published at all.

**Which type of solutions to include?**
Going from the definition of Open Design anything that adheres to this definition can be included in the report. However, not all of them can be found interesting. The OPEN! project already took a choice in this matter, giving an articulation of which solutions are excluded, like amongst others single-board computers. Based on the mission of Re:INVENT only solutions for providing basic human needs in housing/shelter, agri/food, energy, sanitation, healthcare, mobility, infrastructures and such should be included.

Answering the above questions probably requires an iterative approach, where the criteria are tuned along the way. Perhaps 100% objective and definitive criteria cannot even be given. In the end the report should deliver on its goals towards the target audiences, which should be more important than adhering to strict definitions and criteria. Some arbitrary decisions may be necessary, which could be fine as long as they are made and documented transparently.


 




