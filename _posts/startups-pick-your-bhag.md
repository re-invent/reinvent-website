---
layout: post
title: "Startups, pick your BHAG!"
categories: report
---

5 February 2019
By: Diderik van Wingerden

What are the really Big Hairy Audacious Goals (BHAGs) that startup entrepreneurs can or even should focus on? Silicon Valley rhetoric tells us that startups are all about making things better, do no evil and innovate for human progress. But are the Audacious Goals of space travel and self-driving cars really adressing the most pressing challenges that face humanity?

Let us take a step back to that old yet useful Maslow's hierarchy of needs:

https://www.simplypsychology.org/maslow.html

I believe that just as Maslow's pyramid is aimed on 'first things first' on a personal level, we should focus on 'first things first' on a global level. Why let thousands of people work on and invest billions of dollars in space travel, when every day there are millions of people who go to bed hungry (if they even have a bed)?

That is why this article contains the Big Hairy Audacious Goals that I believe startups, entrepreneurs and innovators should work on.

## BHAG 1: Make sure everyone has food
Have you every tried to study, work, socialize, enjoy yourself while begin hungry? Exactly. When you are hungry, nothing else matters. The [UN estimated in 2016](https://www.worldhunger.org/hunger-quiz/how-many-people-are-hungry-in-the-world/) that 815 million people of the 7.6 billion (1 in 10) are suffering from chronic undernourishment.

## BHAG 2: Make sure everyone has clean water
This one goes neatly with BHAG 1: who would not like to drink something with their food? But it is not just about drinking water, clean water is also needed for hygiene: washing yourself and things you use. [WHO/UNICEF estimated in 2017](https://www.wateraid.org/facts-and-statistics) that 844 million people do not have access to clean water.

## BHAG 3: Make sure everyone has access to a toilet


Chart: 2.4 Billion People Live Without Access to Toilets | The Data Blog
https://blogs.worldbank.org/opendata/chart-24-billion-people-live-without-access-toilets


## BHAG 4: Make sure everyone has adequate housing
Having a house is not only for shelter from the rain, sun, cold or heat. Having a home is a safe-haven, a place to be yourself, to be with your loved ones, a cornerstone for your social and private life. [Habitat estimated in 2015](https://homelessworldcup.org/homelessness-statistics/) that 1.6 billion people lack adequate housing (think of slums with no clean water, sewage, electricity, etc.) and the UN estimated in 2005 that 100 million people are homeless ([more recent estimates](https://yaleglobal.yale.edu/content/cities-grow-worldwide-so-do-numbers-homeless) put this figure at 150 million). 






1.1 billion people still lack electricity. This could be the solution | World Economic Forum
https://www.weforum.org/agenda/2018/06/1-billion-people-lack-electricity-solution-mini-grid-iea/

World Bank and WHO: Half the world lacks access to essential health services, 100 million still pushed into extreme poverty because of health expenses
https://www.who.int/news-room/detail/13-12-2017-world-bank-and-who-half-the-world-lacks-access-to-essential-health-services-100-million-still-pushed-into-extreme-poverty-because-of-health-expenses

UN: Majority of world's population lacks internet access
https://www.upi.com/Top_News/World-News/2017/09/18/UN-Majority-of-worlds-population-lacks-internet-access/6571505782626/



