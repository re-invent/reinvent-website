---
layout: page
title: Designs
permalink: /designs/
---

This page contains a growing list of inspiring Open Design-based initiatives for providing basic human needs in: housing, food, energy, water, sanitation, healthcare, communications and mobility.

**Note: These are *not* projects made by Re:INVENT, but serve as inspiration and reference!**

### SunZilla

>SunZilla is a solar-powered generator that provides a clean and easy-to-use alternative for off-grid electricity supply. Its battery storage ensures a reliable and flexible supply even at night or on less sunny days.

>Easy-to-repair - SunZilla’s technology is openly available. This feature also makes SunZilla easy to sustain and repair.

>Easy-to-use - Pick your energy kit, plug together the modules and SunZilla is ready to be used withing 5 min - without the need for any external help.

>Intelligent - An app gives you insights on your energy production and usage. It also allows you to monitor the SunZilla and to set personal preferences.

>Portable - SunZilla can be stacked and easily transported by one person. The whole system weights only 25 kg.

Website: [sunzilla.de](https://sunzilla.de/)

Note: It seems that the SunZilla project is inactive.

### Wikihouse

>The future of homes, by everyone for everyone

>WikiHouse is an open source project to reinvent the way we make homes.

>It is being developed by architects, designers, engineers, inventors, manufacturers and builders, collaborating to develop the best, simplest, most sustainable, high-performance building technologies, which anyone can use and improve.

>Our aim is for these technologies to become new industry standards; the bricks and mortar of the digital age.

Website: [wikihouse.cc](https://wikihouse.cc/)


Do you have an inspiring project to add? Then feel free to do so via GitLab or send an e-mail to info [at] reinvent.cc.

### Lasersaur

>The Lasersaur is a beautiful laser cutter with an outstanding price-performance ratio. We designed it to fill the need of makers, designers, architects and researchers who want a safe and highly-capable machine. Unlike others it's open source and comes fully loaded with knowledge to run, maintain, and modify.

Website: [lasersaur.com](http://www.lasersaur.com/)

### Original Prusa i3

>The Prusa i3 is an open-source fused deposition modeling 3D printer. Part of the RepRap project, it is the most used 3D printer in the world. The Prusa i3 was designed by Josef Prusa in 2012 [...] The Prusa i3's comparable low cost and ease of construction and modification has made it popular in education and with hobbyists and professionals. Due to the printer being open source there have been many variants produced by companies and individuals worldwide, and like many other RepRap printers the Prusa i3 is capable of printing some of its own parts.

Source: wikipedia.org

Website: [prusa3d.com](https://www.prusa3d.com/)

### e-NABLE

>Volunteers using 3D printers to make radically inexpensive prosthetics for under-served populations around the world.

Website: [e-nable.org](http://e-nable.org/)

### Guifi.net

>guifi·net is a bottom-up, citizenship-driven technological, social and economic project with the objective of creating a free, open and neutral telecommunications network based on a commons model. The development of this common-pool infrastructure eases the access to quality, fair-priced telecommunications in general and broadband Internet connections in particular, for everybody. Moreover, it generates a model for collaborative economic activity based on proximity and sustainability.

Website: [guifi.net](http://guifi.net/)

### Jitsi

>Multi-platform open-source video conferencing

>At Jitsi, we believe every video chat should look and sound amazing, between two people or 200. Whether you want to build your own massively multi-user video conference client, or use ours, all our tools are 100% free, open source, and WebRTC compatible.

Website: [jitsi.org](https://jitsi.org/)

### l'Atelier Paysan

>We are a French-speaking collective of small-scale farmers, employees and agricultural development organisations, gathered together as a cooperative named l’Atelier Paysan, a tool box of farmer-driven technologies and practices. Based on the principle that farmers are themselves innovators, we have been collaboratively developing methods and practices to reclaim farming skills and achieve self-sufficiency in relation to the tools and machinery used in organic farming.

Website: [latelierpaysan.org](https://www.latelierpaysan.org/English)

### Open Source Imaging

>The Open Source Imaging Initiative (OSI²) represents a new approach to the development of medical imaging devices, aiming to make the health-care benefits of modern instruments accessible to many more people around the globe. The project will pool the knowledge and experience of many experts in open-source designs for Magnetic Resonance Imaging devices (MRI) which can be built and maintained for a fraction of the price of current instruments.

Website: [opensourceimaging.org](http://www.opensourceimaging.org/2016/05/01/about/)

### XYZ Cargo

>XYZ CARGOs use a completely new way of building functional cycles with a focus on local production in a socially just and environmentally sustainable way.

>The XYZ CARGOs are based on an Open Source construction system called XYZ SPACEFRAME VEHICLES

Website: [xyzcargo.com](http://www.xyzcargo.com/about/)