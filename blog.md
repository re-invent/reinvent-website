---
layout: page
title: Blog
permalink: /blog/
---

{% for post in site.posts %}

{{ post.date | date: "%-d %b %Y" }}

[{{ post.title }}]({{ post.url | prepend: site.baseurl }})
 
{% endfor %}

[Subsribe via RSS]({{ "/feed.xml" | prepend: site.baseurl }})