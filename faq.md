---
layout: page
title: FAQ
permalink: /faq/
---

### What is Open Design?
Open Design is the design, both as process and outcome, of Open (Source) Hardware. The generally accepted definition of Open Source Hardware is created by the Open Source Hardware Association (OSHWA) and is derived from the generally accepted definition of Open Source Software or "Free and Open Source Software" (FOSS or FLOSS).

OSHWA defines Open Source Hardware as (quoted in part):

> Open source hardware is hardware whose design is made publicly available so that anyone can study, modify, distribute, make, and sell the design or hardware based on that design. The hardware’s source, the design from which it is made, is available in the preferred format for making modifications to it. Ideally, open source hardware uses readily-available components and materials, standard processes, open infrastructure, unrestricted content, and open-source design tools to maximize the ability of individuals to make and use hardware. Open source hardware gives people the freedom to control their technology while sharing knowledge and encouraging commerce through the open exchange of designs.

Source: [oshwa.org/definition](https://www.oshwa.org/definition/)

We regard Open Design, Open Source Design, Open Hardware, Open Source Hardware, Free Hardware and Free Libre Open Source Hardware as synonyms. We prefer to use the term "Open Design" as we believe this resonates better with a larger audience than one of the alternatives and it is nice that it can refer to both an open process as an open (licensed) outcome.

### What is a Safe and Just Space for Humanity?
We believe that many of the wicked problems people and nature are enduring at the moment are 'organizational' in nature, meaning that they can be alleviated or even resolved if we would organize ourselves, the social-political-economic system, in a different way. If we do that, we could move towards a 'space' where we do not put more strain on the planet than it can endure (we and nature would be 'safe'), while also being 'just' to all people on the planet, making sure that everyone has their basic needs met. Think of: safety, shelter, food, sanitation, healthcare, energy, communications, education and equality.

For more information, read Doughnut Economics created by Kate Raworth. The "Safe and Just Space for Humanity" is part of her revolutionary holistic economic theory. You can read a review with a summary [here](https://medium.com/@diderik_32187/the-invisible-hole-in-doughnut-economics-7d17cbc5c563). For more information on the design and fundamental problems (at least for the 99% and the planet) of our "Social Operating System" see the work of [Douglas Rushkoff](http://www.rushkoff.com/).

Two of the organizational problems Re:INVENT is addressing are that knowledge is believed to be scarce (copyright, patents, NDAs, secrecy, IPR) and physical resources are believed to be abundant (throwaway consumerism, planned obsolescence, the GDP growth trap, fossil fuel use). We turn this around by understanding and putting into practice that knowledge about technology that allows providing anyone in their basic human needs should be abundantly available as a moral directive, but also as a practical way to create "Permissionless Innovation" (remove friction and competitive waste) if we are to solve these wicked problems any time soon. And by understanding and putting into practice that things should be made in such a way that it allows not only for use, but also for local fabrication with standard components, easy maintenance, repair, customization and improvement, sustained use and finally circular recycling.

### Is Re:INVENT a voluntary organization or a professional entity?
The goal of Re:INVENT is to advance Open Design for a Safe and Just Space for Humanity. We do this in every way we can, both voluntary ("for free") and professional ("paid"). Since we also still need to have monetary income to ensure our livelihoods, this means that voluntary work will move us ahead at an "hours per week" pace. We are always on the lookout for funding for various programs, so we can move faster at "days per week". At this moment Re:INVENT does not have external funding.

### Is Re:INVENT a Makerspace?
Yes and no. The Re:INVENT initiative is more ambitious than creating 'just' a Makerspace. However, to be able to advance Open Design in any practical way, a Makerspace or at least access to one is necessary.

### Is Re:INVENT a startup incubator/accelerator?
At the moment not. But it could be in the future. The aim of Re:INVENT is to advance the development, adoption and use of Open Designs and Open Source technologies. Entrepreneurship and startups are certainly ways to support this aim. We would need to reinvent the way startups and an incubator/accelerator are done, since starting from abundant and open knowledge lends itself for different ways of value creation, business models and scaling up.

### Is Re:INVENT a Tech Shop/Venture Builder?
At the moment not. But it could be in the future. See the answer above. An alternative to having a program to bring in entrepreneurs and startup teams like an incubator/accelerator does, starting ventures from the existing core community could be another path to achieving the goals.

### What is the legal status of Re:INVENT Institute?
At the moment Re:INVENT Institute does not have an official legal status. Re:INVENT exists because we say it exists. We adopt a "just in time, just enough" strategy, which means that a legal entity will be established at the moment that it is necessary. At that moment Re:INVENT Institute will likely become a Foundation ("Stichting") established in The Netherlands. Or it could be beneficial to establish Re:INVENT as a Cooperative.

