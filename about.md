---
layout: page
title: About
permalink: /about/
---

Re:INVENT is an initiative to put innovation, creativity, openness, sharing, transparency and collaboration in practice to ensure that everyone can provide in their basic human needs and move towards a safe and just space for humanity. 

We do this with what we call Permissionless Innovation and Open Source Design. We reinvent product development, start-ups, intellectual property, ownership and scaling up for impact.

Re:INVENT is an initiative of [Diderik van Wingerden](https://twitter.com/diderikvw). At the moment we are a small community, working for the good cause. 

When making something, our approach is "open from the first commit" or what is also called "release early, release often". That is what you see with this website. Instead of spending months designing the perfect website, we put something up & running in one day. And then iterate from that. This website is built using GitLab Pages using Jekyll. All source is [published openly on GitLab](https://gitlab.com/re-invent/reinvent-website), so you can see it all and step right in!

We are looking for people and organizations that are front-runners in innovation and solutions development, who do not just scout and consume existing products, but want to take an active role in co-creating them.

Are you interested to know more? Or do you know someone or an organization that we should talk to? Then [contact us](mailto:info [at] reinvent.cc) (replace [at] for @ without spaces).

All content on this website is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/), unless stated otherwise.

## Inspiration

Taking the Re:INVENT initiative was inspired by:

### POC21

> POC21 was an innovation camp that took place in Chateau de Millemont near Paris. It lasted for five weeks from August 15, 2015 until September 20, 2015. POC21 innovation camp hosted 100 makers, designers and innovators to develop a Proof of Concept of a truly sustainable society. 

Source: Wikipedia.org

Website: [poc21.cc](http://www.poc21.cc/)

### Open Souce Ecology

> We’re developing open source industrial machines that can be made for a fraction of commercial costs, and sharing our designs online for free. The goal of Open Source Ecology is to create an open source economy – an efficient economy which increases innovation by open collaboration.

Website: [opensourceecology.org](https://www.opensourceecology.org/)

### Good Tech

> The Good Tech Alliance fosters a community of makers, designers, engineers and scientists who innovate, develop and prototype good tech. In the long term the aim is to substitute consumer and household goods with good tech alternatives.

Website: [goodtech.cc](http://goodtech.cc/)

### De WAR

> De War is a breeding ground for art, technology, science and sustainability Amersfoort.

The people at De WAR started the world's first grass-roots no-subsidy bootstrapped FabLab consisting of Open Source machines and aimed at sustainability. They also have a yearly grass-roots event called Koppelting:

> Koppelting is an annual grassroots festival about peer production and free/libre alternatives for society. It is filled with projects, lectures, debates and workshops, and is co-created by the attendees.

Websites: [dewar.nl](http://dewar.nl/en/home), [fablabamersfoort.nl](http://fablabamersfoort.nl/en) [koppelting.org](http://www.koppelting.org/).

### FieldReady

> We believe that by making useful things, we can make the world a better place. We do this by working in emergencies and reconstruction, using a range of technologies and engaging people in new ways. The impact of this is more people helped - faster, cheaper and better. 

Website: [fieldready.org](https://www.fieldready.org/)

### Humanitarian Makers

> We are makers – designers, engineers, technicians, logistics experts, supply chain gurus, biomimics, and other professionals from around the world – who desire to create additional social impact with our expertise. 

Website: [humanitarianmakers.org](https://www.humanitarianmakers.org/)

It is our challenge to humbly find our place among these and so many more initiatives; to connect, work together and move our common cause forward.