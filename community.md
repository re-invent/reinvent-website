---
layout: page
title: Community
permalink: /community/
---

At the moment we are learning about existing related initiatives, talking to people and figuring out the added value Re:INVENT can have in the emerging ecosystem. Also see the [About page](/about).

Our aim is to collaborate and integrate with existing communities, supporting and enhancing those. Our aim is not to create a competitive community next to the existing ones.

We are connecting to and exploring collaboration with:
- [The global Fab Labs community](https://fablabs.io/)
- [MakerNet Alliance](http://www.makernetalliance.org/)
- [Latra](http://latra.gr/)
- [The Spindle](https://thespindle.org/)
- [Open Source Ecology Germany](https://opensourceecology.de/)
- [Humanitarian Makers](http://humanitarianmakers.org/)
- [Rotterdam University of Applied Sciences](https://www.rotterdamuas.com/)
- [DCHI](http://dchi.nl/)

Are you interested in what Re:INVENT is doing and would you like to be kept 'in the loop'? Then send an e-mail to [info [at] reinvent.cc](mailto:info [at] reinvent.cc) (replace [at] for @ without spaces).

Are you looking for an internship? Then please send your CV and motivation letter to [info [at] reinvent.cc](mailto:info [at] reinvent.cc) (replace [at] for @ without spaces).